# CryptoExplorer

Crypto Explorer

## Bitcoin Explorer
[API Reference](https://blockexplorer.com/api-ref)

### Usage
```
BitcoinExplorer explorer = new BitcoinExplorer();
```

## Dogecoin Explorer
[API Reference](https://dogechain.info/api/blockchain_api)

### Usage
```
DogecoinRestExplorer explorer = new DogecoinRestExplorer();
```
