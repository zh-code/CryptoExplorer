﻿using BitcoinBlockExplorer.Model;
using Newtonsoft.Json;
using QueryString;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace BitcoinBlockExplorer
{
    public class BitcoinExplorer
    {
        private HttpClient client;
        private JsonSerializerSettings jsonSetting;

        public BitcoinExplorer()
        {
            client = new HttpClient
            {
                BaseAddress = new Uri("https://blockexplorer.com/")
            };
            client.DefaultRequestHeaders.Add("User-Agent", "zh-code.com .Net Wrapper");
            jsonSetting = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };
        }

        public void Production()
        {
            client.BaseAddress = new Uri("https://blockexplorer.com/");
        }

        public void TestNet()
        {
            client.BaseAddress = new Uri("https://testnet.blockexplorer.com/");
        }

        public async Task<Block> Block(string hash) => await HandleRequest<Block>($"api/block/{hash}");

        public async Task<RawBlock> RawBlock(string hash) => await HandleRequest<RawBlock>($"api/rawblock/{hash}");

        public async Task<BlockIndex> BlockIndex(int height) => await HandleRequest<BlockIndex>($"api/block-index/{height}");

        public async Task<Transaction> Transaction(string txid) => await HandleRequest<Transaction>($"api/tx/{txid}");

        public async Task<RawTransaction> RawTransaction(string rawid) => await HandleRequest<RawTransaction>($"api/rawtx/{rawid}");

        public async Task<Address> Address(string address, bool noTxList = false, bool noCache = false)
        {
            QueryStringBuilder qsb = new QueryStringBuilder();
            if (noTxList)
            {
                qsb.Add("noTxList", 1);
            }
            if (noCache)
            {
                qsb.Add("noCache", 1);
            }

            if (qsb.Count() > 0)
            {
                return await HandleRequest<Address>($"api/addr/{address}" + qsb.Build());
            }
            return await HandleRequest<Address>($"api/addr/{address}");
        }

        public async Task<List<UnSpentOutput>> UnSpentOutputs(List<string> addresses) =>
            await HandleRequest<List<UnSpentOutput>>($"api/addrs/{string.Join(",", addresses)}/utxo");

        public async Task<TransactionList> TransactionsByBlocks(string hash) =>
            await HandleRequest<TransactionList>($"api/txs/?block={hash}");

        public async Task<TransactionsByAddresses> TransactionsByAddresses(List<string> addresses, int from = 0, int to = 0)
        {
            QueryStringBuilder qsb = new QueryStringBuilder();
            if (from != 0)
            {
                qsb.Add("from", from);
            }
            if (to != 0)
            {
                qsb.Add("to", to);
            }
            if (qsb.Count() > 0)
            {
                return await HandleRequest<TransactionsByAddresses>($"api/addrs/{string.Join(",", addresses)}/txs" + qsb.Build());
            }
            return await HandleRequest<TransactionsByAddresses>($"api/addrs/{string.Join(",", addresses)}/txs");
        }

        public async Task<string> Send(string signedTxInHex)
        {
            var formContent = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("rawtx", signedTxInHex)
            });

            using (var response = await client.PostAsync("api/tx/send", formContent))
            {
                return JsonConvert.DeserializeObject<Send>(await response.Content.ReadAsStringAsync()).TxId;
            }
        }

        public async Task<StatusInfo> StatusInfo()
        {
            StatusInfoResponse response = await HandleRequest<StatusInfoResponse>($"api/status?q=getInfo");

            return response.info;
        }

        public async Task<decimal> StatusDifficulty()
        {
            Difficulty response = await HandleRequest<Difficulty>($"api/status?q=getDifficulty");

            return response.difficulty;
        }

        public async Task<string> StatusBestBlockHash()
        {
            BestBlockHash response = await HandleRequest<BestBlockHash>($"api/status?q=getBestBlockHash");

            return response.bestblockhash;
        }

        private async Task<T> HandleRequest<T>(string url) where T : class
        {
            using (var response = await client.GetAsync(url))
            {
                try
                {
                    return JsonConvert.DeserializeObject<T>(await response.Content.ReadAsStringAsync(), jsonSetting);
                }
                catch { return null; }
            }
        }
    }
}