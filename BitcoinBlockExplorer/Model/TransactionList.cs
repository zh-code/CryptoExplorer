﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace BitcoinBlockExplorer.Model
{
    public class TransactionList
    {
        [JsonProperty("pagesTotal")]
        public int PagesTotal { get; set; }

        [JsonProperty("txs")]
        public List<Transaction> Txs { get; set; }
    }
}