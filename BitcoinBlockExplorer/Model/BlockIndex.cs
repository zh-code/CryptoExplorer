﻿using Newtonsoft.Json;

namespace BitcoinBlockExplorer.Model
{
    public class BlockIndex
    {
        [JsonProperty("blockHash")]
        public string BlockHash { get; set; }
    }
}