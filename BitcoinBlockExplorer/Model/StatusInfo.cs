﻿using Newtonsoft.Json;

namespace BitcoinBlockExplorer.Model
{
    public class StatusInfo
    {
        [JsonProperty("version")]
        public int Version { get; set; }

        [JsonProperty("protocolversion")]
        public int Protocolversion { get; set; }

        [JsonProperty("blocks")]
        public int Blocks { get; set; }

        [JsonProperty("timeoffset")]
        public int Timeoffset { get; set; }

        [JsonProperty("connections")]
        public int Connections { get; set; }

        [JsonProperty("proxy")]
        public string Proxy { get; set; }

        [JsonProperty("difficulty")]
        public double Difficulty { get; set; }

        [JsonProperty("testnet")]
        public bool Testnet { get; set; }

        [JsonProperty("relayfee")]
        public double RelayFee { get; set; }

        [JsonProperty("errors")]
        public string Errors { get; set; }

        [JsonProperty("network")]
        public string Network { get; set; }
    }

    internal class StatusInfoResponse
    {
        public StatusInfo info { get; set; }
    }
}