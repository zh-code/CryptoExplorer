﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace BitcoinBlockExplorer.Model
{
    public class Address
    {
        [JsonProperty("addrStr")]
        public string AddrStr { get; set; }

        [JsonProperty("balance")]
        public decimal Balance { get; set; }

        [JsonProperty("balanceSat")]
        public long BalanceSat { get; set; }

        [JsonProperty("totalReceived")]
        public decimal TotalReceived { get; set; }

        [JsonProperty("totalReceivedSat")]
        public long TotalReceivedSat { get; set; }

        [JsonProperty("totalSent")]
        public decimal TotalSent { get; set; }

        [JsonProperty("totalSentSat")]
        public long TotalSentSat { get; set; }

        [JsonProperty("unconfirmedBalance")]
        public decimal UnconfirmedBalance { get; set; }

        [JsonProperty("unconfirmedBalanceSat")]
        public long UnconfirmedBalanceSat { get; set; }

        [JsonProperty("unconfirmedTxApperances")]
        public int UnconfirmedTxApperances { get; set; }

        [JsonProperty("txApperances")]
        public int TxApperances { get; set; }

        [JsonProperty("transactions")]
        public List<string> Transactions { get; set; }
    }
}