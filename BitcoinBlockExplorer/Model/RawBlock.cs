﻿using Newtonsoft.Json;

namespace BitcoinBlockExplorer.Model
{
    public class RawBlock
    {
        [JsonProperty("rawblock")]
        public string RawBlockHash { get; set; }
    }
}