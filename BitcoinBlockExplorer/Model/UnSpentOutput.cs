﻿using Newtonsoft.Json;

namespace BitcoinBlockExplorer.Model
{
    public class UnSpentOutput
    {
        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("txid")]
        public string TxId { get; set; }

        [JsonProperty("vout")]
        public int Vout { get; set; }

        [JsonProperty("scriptPubKey")]
        public string ScriptPubKey { get; set; }

        [JsonProperty("amount")]
        public decimal Amount { get; set; }

        [JsonProperty("satoshis")]
        public long Satoshis { get; set; }

        [JsonProperty("height")]
        public int Height { get; set; }

        [JsonProperty("confirmations")]
        public int Confirmations { get; set; }
    }
}