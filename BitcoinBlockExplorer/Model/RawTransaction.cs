﻿using Newtonsoft.Json;

namespace BitcoinBlockExplorer.Model
{
    public class RawTransaction
    {
        [JsonProperty("rawtx")]
        public string RawTx { get; set; }
    }
}