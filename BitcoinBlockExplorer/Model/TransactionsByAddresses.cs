﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace BitcoinBlockExplorer.Model
{
    public class TransactionsByAddresses
    {
        [JsonProperty("totalItems")]
        public int TotalItems { get; set; }

        [JsonProperty("from")]
        public int From { get; set; }

        [JsonProperty("to")]
        public int To { get; set; }

        [JsonProperty("items")]
        public List<Transaction> Items { get; set; }
    }
}