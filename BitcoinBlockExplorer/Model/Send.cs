﻿using Newtonsoft.Json;

namespace BitcoinBlockExplorer.Model
{
    internal class Send
    {
        [JsonProperty("txid")]
        public string TxId { get; set; }
    }
}