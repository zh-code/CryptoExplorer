﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace BitcoinBlockExplorer.Model
{
    public class Transaction
    {
        [JsonProperty("txid")]
        public string TxId { get; set; }

        [JsonProperty("version")]
        public int Version { get; set; }

        [JsonProperty("locktime")]
        public int LockTime { get; set; }

        [JsonProperty("vin")]
        public List<VIN> Vin { get; set; }

        [JsonProperty("vout")]
        public List<VOUT> Vout { get; set; }

        [JsonProperty("blockhash")]
        public string BlockHash { get; set; }

        [JsonProperty("blockheight")]
        public int BlockHeight { get; set; }

        [JsonProperty("confirmations")]
        public int Confirmations { get; set; }

        [JsonProperty("time")]
        public int Time { get; set; }

        [JsonProperty("blocktime")]
        public int BlockTime { get; set; }

        [JsonProperty("isCoinBase")]
        public bool IsCoinBase { get; set; }

        [JsonProperty("valueOut")]
        public decimal ValueOut { get; set; }

        [JsonProperty("size")]
        public int Size { get; set; }

        [JsonProperty("valueIn")]
        public decimal ValueIn { get; set; }

        [JsonProperty("fees")]
        public decimal Fees { get; set; }

        public class VIN
        {
            [JsonProperty("coinbase")]
            public string Coinbase { get; set; }

            [JsonProperty("sequence")]
            public long Sequence { get; set; }

            [JsonProperty("n")]
            public int N { get; set; }

            [JsonProperty("txid")]
            public string TxId { get; set; }

            [JsonProperty("vout")]
            public int Vout { get; set; }

            [JsonProperty("scriptSig")]
            public ScriptSignature ScriptSig { get; set; }

            [JsonProperty("addr")]
            public string Addr { get; set; }

            [JsonProperty("valueSat")]
            public long ValueSat { get; set; }

            [JsonProperty("value")]
            public decimal Value { get; set; }

            [JsonProperty("doubleSpentTxID")]
            public object DoubleSpentTxID { get; set; }
        }

        public class SCRIPTPUBKEY
        {
            [JsonProperty("hex")]
            public string Hex { get; set; }

            [JsonProperty("asm")]
            public string Asm { get; set; }

            [JsonProperty("addresses")]
            public List<string> Addresses { get; set; }

            [JsonProperty("type")]
            public string Type { get; set; }
        }

        public class VOUT
        {
            [JsonProperty("value")]
            public string Value { get; set; }

            [JsonProperty("n")]
            public int N { get; set; }

            [JsonProperty("scriptPubKey")]
            public SCRIPTPUBKEY ScriptPubKey { get; set; }

            [JsonProperty("spentTxId")]
            public string SpentTxId { get; set; }

            [JsonProperty("spentIndex")]
            public int SpentIndex { get; set; }

            [JsonProperty("spentHeight")]
            public int SpentHeight { get; set; }
        }

        public class ScriptSignature
        {
            [JsonProperty("hex")]
            public string Hex { get; set; }

            [JsonProperty("asm")]
            public string Asm { get; set; }
        }
    }
}