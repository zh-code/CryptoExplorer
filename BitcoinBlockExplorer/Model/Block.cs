﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace BitcoinBlockExplorer.Model
{
    public class Block
    {
        [JsonProperty("hash")]
        public string Hash { get; set; }

        [JsonProperty("size")]
        public int Size { get; set; }

        [JsonProperty("height")]
        public int Height { get; set; }

        [JsonProperty("version")]
        public int Version { get; set; }

        [JsonProperty("merkleroot")]
        public string Merkleroot { get; set; }

        [JsonProperty("tx")]
        public List<string> Tx { get; set; }

        [JsonProperty("time")]
        public long Time { get; set; }

        [JsonProperty("nonce")]
        public long Nonce { get; set; }

        [JsonProperty("bits")]
        public string Bits { get; set; }

        [JsonProperty("difficulty")]
        public decimal Difficulty { get; set; }

        [JsonProperty("chainwork")]
        public string Chainwork { get; set; }

        [JsonProperty("confirmations")]
        public int Confirmations { get; set; }

        [JsonProperty("previousblockhash")]
        public string PreviousBlockHash { get; set; }

        [JsonProperty("nextblockhash")]
        public string NextBlockHash { get; set; }

        [JsonProperty("reward")]
        public decimal Reward { get; set; }

        [JsonProperty("isMainChain")]
        public bool IsMainChain { get; set; }

        [JsonProperty("poolInfo")]
        public PoolInfo Pool { get; set; }

        public class PoolInfo
        {
            [JsonProperty("poolName")]
            public string PoolName { get; set; }

            [JsonProperty("url")]
            public string Url { get; set; }
        }
    }
}