﻿using BitcoinBlockExplorer;
using NUnit.Framework;
using System.Linq;
using System.Threading.Tasks;

namespace ExplorerTest
{
    public class BitcoinExplorerTest
    {
        private BitcoinExplorer explorer;

        [SetUp]
        public void Init()
        {
            explorer = new BitcoinExplorer();
        }

        [Test]
        [TestCase("0000000000000000001df2c01c3abb5ab7d21523602d1721f8aa4e4c4f035212")]
        [TestCase("00000000000000000004d84999ac7e98f7f274064edcd8fd010e2b7d1841cee9")]
        public async Task Block(string hash)
        {
            var block = await explorer.Block(hash);

            Assert.IsNotNull(block);
        }

        [Test]
        [TestCase("0000000000000000001df2c01c3abb5ab7d21523602d1721f8aa4e4c4f035212")]
        [TestCase("00000000000000000004d84999ac7e98f7f274064edcd8fd010e2b7d1841cee9")]
        public async Task RawBlock(string hash)
        {
            var rawblock = await explorer.RawBlock(hash);

            Assert.IsNotNull(rawblock);
        }

        [Test]
        [TestCase(571953)]
        [TestCase(571952)]
        public async Task BlockIndex(int index)
        {
            var blockindex = await explorer.BlockIndex(index);

            Assert.IsNotNull(blockindex);
        }

        [Test]
        [TestCase("87cd4c4af6a9144b9809afe766396cc88bc32e2a0481b97c4dbb1429885f5940")]
        [TestCase("234faf59810f6efc677c7d9670204314bc802ec9a5f26a4f8e2a8cdaa951246b")]
        public async Task Transaction(string txid)
        {
            var tx = await explorer.Transaction(txid);

            Assert.IsNotNull(tx);
        }

        [Test]
        [TestCase("87cd4c4af6a9144b9809afe766396cc88bc32e2a0481b97c4dbb1429885f5940")]
        [TestCase("234faf59810f6efc677c7d9670204314bc802ec9a5f26a4f8e2a8cdaa951246b")]
        public async Task RawTransaction(string txid)
        {
            var rawtx = await explorer.RawTransaction(txid);

            Assert.IsNotNull(rawtx);
        }

        [Test]
        [TestCase("19SokJG7fgk8iTjemJ2obfMj14FM16nqzj", false, false)]
        [TestCase("19SokJG7fgk8iTjemJ2obfMj14FM16nqzj", true, false)]
        [TestCase("19SokJG7fgk8iTjemJ2obfMj14FM16nqzj", false, true)]
        [TestCase("19SokJG7fgk8iTjemJ2obfMj14FM16nqzj", false, false)]
        public async Task Address(string address, bool noTxList, bool noCache)
        {
            var addr = await explorer.Address(address, noTxList, noCache);

            Assert.IsNotNull(addr);

            if (!noTxList)
            {
                Assert.GreaterOrEqual(addr.Transactions.Count, 0);
            }
        }

        [Test]
        [TestCase("1Hyvn83yUY2MmnEN1bL1oGBr8eoUEqxoKL")]
        [TestCase("1Hyvn83yUY2MmnEN1bL1oGBr8eoUEqxoKL", "3JfGsKiFqCrRvwghoiKnuU8E7nmP8GvS9p")]
        public async Task UnSpentOutputs(params string[] addresses)
        {
            var outputs = await explorer.UnSpentOutputs(addresses.ToList());

            Assert.IsNotNull(outputs);
            Assert.GreaterOrEqual(outputs.Count, 0);
        }

        [Test]
        [TestCase("0000000000000000001df2c01c3abb5ab7d21523602d1721f8aa4e4c4f035212")]
        [TestCase("00000000000000000004d84999ac7e98f7f274064edcd8fd010e2b7d1841cee9")]
        public async Task TransactionsByBlocks(string hash)
        {
            var txs = await explorer.TransactionsByBlocks(hash);

            Assert.IsNotNull(txs);
            Assert.GreaterOrEqual(txs.Txs.Count, 0);
        }

        [Test]
        [TestCase(0, 0, "1Hyvn83yUY2MmnEN1bL1oGBr8eoUEqxoKL")]
        [TestCase(0, 1, "1Hyvn83yUY2MmnEN1bL1oGBr8eoUEqxoKL", "3JfGsKiFqCrRvwghoiKnuU8E7nmP8GvS9p")]
        [TestCase(1, 2, "1Hyvn83yUY2MmnEN1bL1oGBr8eoUEqxoKL", "3JfGsKiFqCrRvwghoiKnuU8E7nmP8GvS9p")]
        [TestCase(1, 0, "1Hyvn83yUY2MmnEN1bL1oGBr8eoUEqxoKL", "3JfGsKiFqCrRvwghoiKnuU8E7nmP8GvS9p")]
        public async Task TransactionsByAddresses(int from, int to, params string[] addresses)
        {
            var txs = await explorer.TransactionsByAddresses(addresses.ToList(), from, to);

            Assert.IsNotNull(txs);
            Assert.GreaterOrEqual(txs.Items.Count, 0);
        }

        [Test]
        public async Task StatusInfo()
        {
            var info = await explorer.StatusInfo();

            Assert.IsNotNull(info);
        }

        [Test]
        public async Task StatusDifficulty()
        {
            var difficulty = await explorer.StatusDifficulty();

            Assert.Greater(difficulty, 0);
        }

        [Test]
        public async Task StatusBestBlockHash()
        {
            var hash = await explorer.StatusBestBlockHash();

            Assert.IsFalse(string.IsNullOrEmpty(hash));
        }
    }
}