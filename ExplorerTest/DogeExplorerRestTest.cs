﻿using DogecoinBlockExplorer;
using NUnit.Framework;
using System.Threading.Tasks;

namespace ExplorerTest
{
    public class DogeExplorerRestTest
    {
        private DogecoinRestExplorer explorer;

        [SetUp]
        public void Init()
        {
            explorer = new DogecoinRestExplorer();
        }

        [Test]
        [TestCase("DLLTusCkfyQAMzGpiugYNHaxjJjfiwngUN")]
        [TestCase("DTM76kkL5QXSmf1V6nLjaxoZYKs1fynFiS")]
        public async Task Balance(string address)
        {
            var balance = await explorer.Balance(address);

            Assert.IsNotNull(balance);
            Assert.IsTrue(balance.Success);
        }

        [Test]
        [TestCase("DLLTusCkfyQAMzGpiugYNHaxjJjfiwngUN")]
        [TestCase("DTM76kkL5QXSmf1V6nLjaxoZYKs1fynFiS")]
        public async Task AmountReceived(string address)
        {
            var amountReceived = await explorer.AmountReceived(address);

            Assert.IsNotNull(amountReceived);
            Assert.IsTrue(amountReceived.Success);
        }

        [Test]
        [TestCase("DLLTusCkfyQAMzGpiugYNHaxjJjfiwngUN")]
        [TestCase("DTM76kkL5QXSmf1V6nLjaxoZYKs1fynFiS")]
        public async Task AmountSent(string address)
        {
            var amountSent = await explorer.AmountSent(address);

            Assert.IsNotNull(amountSent);
            Assert.IsTrue(amountSent.Success);
        }

        [Test]
        [TestCase("DLLTusCkfyQAMzGpiugYNHaxjJjfiwngUN")]
        [TestCase("DTM76kkL5QXSmf1V6nLjaxoZYKs1fynFiS")]
        public async Task UnspentOutputs(string address)
        {
            var unspentOutput = await explorer.UnspentOutput(address);

            Assert.IsNotNull(unspentOutput);
            Assert.IsTrue(unspentOutput.Success);
            Assert.GreaterOrEqual(unspentOutput.UnspentOutputs.Count, 0);
        }

        [Test]
        [TestCase("d5d9eaaf03a0525b8ce61001c1b7ad5296281b6880e50174e40ed4abb848d804")]
        [TestCase("d651bc1e5b825ae1a030ef271f1a72a384c39905b9ab761dd0f22f31f0a8d307")]
        public async Task GetTransaction(string transactionHash)
        {
            var transaction = await explorer.Transaction(transactionHash);

            Assert.IsNotNull(transaction);
            Assert.IsTrue(transaction.Success);
            Assert.IsNotNull(transaction.Transaction);
            Assert.GreaterOrEqual(transaction.Transaction.Inputs.Count, 0);
            Assert.GreaterOrEqual(transaction.Transaction.Outputs.Count, 0);
        }

        [Test]
        [TestCase("2691306")]
        [TestCase("bfbb156cd313d7d79c8c7de0b77b48ebbe36ae093c7176202ff3da0f8a196713")]
        public async Task GetBlock(string heightOrHash)
        {
            var block = await explorer.Block(heightOrHash);

            Assert.IsNotNull(block);
            Assert.IsTrue(block.Success);
            Assert.IsNotNull(block.Block);
            Assert.GreaterOrEqual(block.Block.Txs.Count, 0);
        }
    }
}