﻿using DogecoinBlockExplorer.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace DogecoinBlockExplorer
{
    public class DogecoinRestExplorer
    {
        private HttpClient client;
        private JsonSerializerSettings jsonSetting;

        public DogecoinRestExplorer()
        {
            client = new HttpClient
            {
                BaseAddress = new Uri("https://dogechain.info/api/v1/")
            };
            client.DefaultRequestHeaders.Add("User-Agent", "zh-code.com .Net Wrapper");
            jsonSetting = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };
        }

        public async Task<BalanceResponse> Balance(string address) =>
            await HandleRequest<BalanceResponse>($"address/balance/{address}");

        public async Task<AmountReceivedResponse> AmountReceived(string address) =>
            await HandleRequest<AmountReceivedResponse>($"address/received/{address}");

        public async Task<AmountSentResponse> AmountSent(string address) =>
            await HandleRequest<AmountSentResponse>($"address/sent/{address}");

        public async Task<UnspentOutputResponse> UnspentOutput(string address) =>
            await HandleRequest<UnspentOutputResponse>($"unspent/{address}");

        public async Task<TransactionResponse> Transaction(string transactionHash) =>
            await HandleRequest<TransactionResponse>($"transaction/{transactionHash}");

        public async Task<PushTransactionResponse> PushTransaction(string signedTxInHex)
        {
            var formContent = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("tx", signedTxInHex)
            });

            using (var response = await client.PostAsync("pushtx", formContent))
            {
                return JsonConvert.DeserializeObject<PushTransactionResponse>(await response.Content.ReadAsStringAsync());
            }
        }

        public async Task<BlockResponse> Block(string heightOrHash) =>
            await HandleRequest<BlockResponse>($"block/{heightOrHash}");

        private async Task<T> HandleRequest<T>(string url) where T : class
        {
            using (var response = await client.GetAsync(url))
            {
                try
                {
                    return JsonConvert.DeserializeObject<T>(await response.Content.ReadAsStringAsync(), jsonSetting);
                }
                catch { return null; }
            }
        }
    }
}