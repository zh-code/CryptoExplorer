﻿using Newtonsoft.Json;

namespace DogecoinBlockExplorer.Model
{
    public class AmountSentResponse : ResponseBase
    {
        [JsonProperty("sent")]
        public decimal Sent { get; set; }
    }
}