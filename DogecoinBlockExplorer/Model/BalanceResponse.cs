﻿using Newtonsoft.Json;

namespace DogecoinBlockExplorer.Model
{
    public class BalanceResponse : ResponseBase
    {
        [JsonProperty("balance")]
        public decimal Balance { get; set; }
    }
}