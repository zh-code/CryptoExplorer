﻿using Newtonsoft.Json;

namespace DogecoinBlockExplorer.Model
{
    public class AmountReceivedResponse : ResponseBase
    {
        [JsonProperty("received")]
        public decimal Received { get; set; }
    }
}