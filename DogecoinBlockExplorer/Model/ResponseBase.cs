﻿using Newtonsoft.Json;

namespace DogecoinBlockExplorer.Model
{
    public abstract class ResponseBase
    {
        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("error")]
        public string Error { get; set; }
    }
}