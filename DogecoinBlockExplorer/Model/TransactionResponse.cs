﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace DogecoinBlockExplorer.Model
{
    public class TransactionResponse : ResponseBase
    {
        [JsonProperty("transaction")]
        public TransactionModel Transaction { get; set; }

        public class ScriptSignature
        {
            [JsonProperty("hex")]
            public string Hex { get; set; }
        }

        public class PreviousOutput
        {
            [JsonProperty("hash")]
            public string Hash { get; set; }

            [JsonProperty("pos")]
            public int Position { get; set; }
        }

        public class Output
        {
            [JsonProperty("pos")]
            public int Position { get; set; }

            [JsonProperty("value")]
            public decimal Value { get; set; }

            [JsonProperty("type")]
            public string Type { get; set; }

            [JsonProperty("address")]
            public string Address { get; set; }
        }

        public class Input : Output
        {
            [JsonProperty("scriptSig")]
            public ScriptSignature ScriptSig { get; set; }

            [JsonProperty("previous_output")]
            public PreviousOutput PreviousOutput { get; set; }
        }

        public class TransactionModel
        {
            [JsonProperty("hash")]
            public string Hash { get; set; }

            [JsonProperty("confirmations")]
            public int Confirmations { get; set; }

            [JsonProperty("size")]
            public int Size { get; set; }

            [JsonProperty("version")]
            public int Version { get; set; }

            [JsonProperty("locktime")]
            public long Locktime { get; set; }

            [JsonProperty("block_hash")]
            public string BlockHash { get; set; }

            [JsonProperty("time")]
            public long Time { get; set; }

            [JsonProperty("inputs_n")]
            public int InputsN { get; set; }

            [JsonProperty("inputs")]
            public List<Input> Inputs { get; set; }

            [JsonProperty("inputs_value")]
            public decimal InputsValue { get; set; }

            [JsonProperty("outputs_n")]
            public int OutputsN { get; set; }

            [JsonProperty("outputs")]
            public List<Output> Outputs { get; set; }

            [JsonProperty("outputs_value")]
            public decimal OutputsValue { get; set; }

            [JsonProperty("fee")]
            public decimal Fee { get; set; }
        }
    }
}