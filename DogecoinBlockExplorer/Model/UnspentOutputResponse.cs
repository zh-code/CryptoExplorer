﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace DogecoinBlockExplorer.Model
{
    public class UnspentOutputResponse : ResponseBase
    {
        [JsonProperty("unspent_outputs")]
        public List<UnspentOutput> UnspentOutputs { get; set; }

        public class UnspentOutput
        {
            [JsonProperty("tx_hash")]
            public string TxHash { get; set; }

            [JsonProperty("tx_output_n")]
            public int TxOutputN { get; set; }

            [JsonProperty("script")]
            public string Script { get; set; }

            [JsonProperty("value")]
            public long Value { get; set; }

            [JsonProperty("confirmations")]
            public int Confirmations { get; set; }
        }
    }
}