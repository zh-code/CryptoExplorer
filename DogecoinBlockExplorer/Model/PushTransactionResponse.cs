﻿using Newtonsoft.Json;

namespace DogecoinBlockExplorer.Model
{
    public class PushTransactionResponse : ResponseBase
    {
        [JsonProperty("tx_hash")]
        public string TxHash { get; set; }
    }
}