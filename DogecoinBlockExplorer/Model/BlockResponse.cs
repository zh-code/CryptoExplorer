﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace DogecoinBlockExplorer.Model
{
    public class BlockResponse : ResponseBase
    {
        [JsonProperty("block")]
        public BlockModel Block { get; set; }

        public class BlockModel
        {
            [JsonProperty("hash")]
            public string Hash { get; set; }

            [JsonProperty("height")]
            public int Height { get; set; }

            [JsonProperty("previous_block_hash")]
            public string PreviousBlockHash { get; set; }

            [JsonProperty("next_block_hash")]
            public string NextBlockHash { get; set; }

            [JsonProperty("is_orphan")]
            public bool IsOrphan { get; set; }

            [JsonProperty("difficulty")]
            public double Difficulty { get; set; }

            [JsonProperty("time")]
            public long Time { get; set; }

            [JsonProperty("confirmations")]
            public int Confirmations { get; set; }

            [JsonProperty("merkleroot")]
            public string Merkleroot { get; set; }

            [JsonProperty("num_txs")]
            public int NumTxs { get; set; }

            [JsonProperty("value_in")]
            public decimal ValueIn { get; set; }

            [JsonProperty("value_out")]
            public decimal ValueOut { get; set; }

            [JsonProperty("version")]
            public int Version { get; set; }

            [JsonProperty("average_coin_age")]
            public decimal AverageCoinAge { get; set; }

            [JsonProperty("nonce")]
            public long nonce { get; set; }

            [JsonProperty("txs")]
            public List<string> Txs { get; set; }
        }
    }
}